#include<iostream>
#include<vector>
using namespace std;


template<class K, class V>
struct HashNode
{
public:
	pair<K, V> _kv;
	HashNode* _next;

	HashNode(const pair<K,V>&kv)
		:
		_kv(kv),
		_next(nullptr)
	{}
};
template<class K,class V,class hash=hash>
class Hashtable
{
public:
	typedef HashNode Node;

private:
	vector<Node*> 
};