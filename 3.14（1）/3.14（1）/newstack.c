#define CRT_SECURE_NO_WARNINGS 1
#include"newstack.h"

void STInit(Stack* p)
{
	assert(p);
	p->arr = NULL;
	p->capacity = p->top = 0;
}

void STDestroy(Stack*p)
{
	assert(p);
	free(p->arr);
	//
	//这里不要忘了free完了 要指向NULL；
	p->arr = NULL;
	p->capacity = p->top = 0;
}

void put(Stack* p, STDataType x)
{
	assert(p);
	if (p->capacity == p->top)
	{
		//内存满了

		int newcapacity = p->capacity == 0 ? 4: 2 * p->capacity;
		//扩容

		STDataType* tmp = (STDataType*)realloc(p->arr, newcapacity*sizeof(Stack));
		if (tmp == NULL)
		{

			perror("realloc fail!");
			exit(1);
		}

		p->capacity = newcapacity;
		p->arr = tmp;

	}
	p->arr[p->top] = x;
	p->top++;
}

void pop(Stack* p)
{
	assert(p);
	assert(!empty(p));
	p->top--;
}
int empty(Stack* p)
{
	//如果为0  就是空的 返回 1 
	assert(p);
	return p->top == 0;
}
//void newput(Stack* p1, Stack* p2, STDataType x)
//{
//	if (p1->top == 0 && p2->top == 0)
//	{
//		put(p1, x);
//	}
//
//	if (p1->top == 0 && p2->top != 0)
//	{
//		//其中p1为空
//		put(p1,x);
//		int sz = p2->top;
//		memmove(p1->arr[1], p2->arr, sz* sizeof(STDataType));
//		p2->top = 0;
//	}
//	else if (p1->top != 0 && p2->top == 0)
//	{
//		put(p2, x);
//		int sz = p1->top;
//		memmove(p2->arr[1], p1->arr, sz * sizeof(STDataType));
//		p1->top = 0;
//	}
//}
void newput(Stack* p1, Stack* p2, STDataType x)
{
	if (p1->top == 0 && p2->top == 0)
	{
		put(p1, x);
		return;
	}
	if (p1->top == 0 && p2->top != 0)
	{
		//其中p1为空
		put(p1, x);
		//int sz = sizeof(p2->arr) / sizeof(STDataType);
		memcpy(&(p1->arr[1]), p2->arr, p2->top * sizeof(STDataType));
		free(p2->arr);
		p2->arr=NULL;
		p2->capacity = p2->top = 0;
	}
	else if (p1->top != 0 && p2->top == 0)
	{
		put(p2, x);
		memcpy(&(p2->arr[1]), p1->arr, p1->top * sizeof(STDataType));
		free(p1->arr);
		p1->arr = NULL;
		p1->capacity = p1->top = 0;
	}
}
