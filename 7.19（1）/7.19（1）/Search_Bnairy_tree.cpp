//#include<iostream>
//using namespace std;
//
//template<class K>
//struct BSTreeNode
//{
//	K _key;
//	BSTreeNode<K>* _left;
//	BSTreeNode<K>* _right;
//
//	BSTreeNode(const K&k)
//		:
//		_key(k),
//		_left(nullptr),
//		_right(nullptr)
//	{}
//};
//
//template<class K>
//class BSTree
//{
//	//重定义 
//	typedef BSTreeNode<K> Node;
//
//public:
//
//	bool insert(const K&key)
//	{
//		//前置处理 如果为空
//		if (_root == nullptr)
//		{
//			_root = new Node(key);
//			return true;
//		}
//
//		//如果不为空
//		Node* cur = _root;
//		Node* parent = _root;
//		while(cur)
//		{
//			if (key > cur->_key)
//			{
//				parent = cur;
//				cur = cur->_right;
//			}
//			else if(key<cur->_key)
//			{
//				parent = cur;
//				cur = cur->_left;
//			}
//			else
//			{
//				return true;
//			}
//		}
//		cur = new Node(key);
//		if(key>parent->_key)
//		{
//			parent->_right = cur;
//		}
//		else
//		{
//			parent->_left = cur;
//		}
//	}
//	void printorder()
//	{
//		_printorder(_root);
//		cout << endl;
//
//	}
//	bool erase(const K& key)
//	{
//		Node* parent = nullptr;
//		Node* cur = _root;
//		while (cur)
//		{
//			if (key > cur->_key)
//			{
//				parent = cur;
//				cur = cur->_right;
//			}
//			else if (key < cur->_key)
//			{
//				parent = cur;
//				cur = cur->_left;
//			}
//			else
//			{
//				//找到了 可以删除了
//
//				//只有一个孩子或者没有孩子就可以直接进行删除
//				if (cur->_left == nullptr )
//				{
//					if (parent == nullptr)
//					{
//						cur = cur->_right;
//       				}
//					if (parent->_left == cur)
//					{
//						parent->_left = cur->_right;
//					}
//					else if (parent->_right == cur)
//					{
//						parent->_right = cur->_right;
//					}
//					delete cur;
//					return true;
//				}
//				else if (cur->_right == nullptr)
//				{
//					if(parent->)
//					if (parent->_left == cur)
//					{
//						parent->_left = cur->_left;
//					}
//					else if (parent->_right == cur)
//					{
//						parent->_right = cur->_left;
//					}
//					delete cur;
//					return true;
//				}
//				else
//				{
//
//					//找一个替代节点  要求左子树的最大 右子树的最小
//					Node* RightMin = cur->_right;
//					Node* RightMinPar = cur;
//					while (RightMin->_left)
//					{
//						RightMinPar = RightMin;
//						RightMin = RightMin->_left;
//					}
//					//算是找到了
//					cur->_key = RightMin->_key;
//
//					if (RightMinPar->_left = RightMin)
//					{
//						RightMinPar->_left = RightMin->_right;
//					}
//					else if(RightMinPar->_right=RightMin)
//					{
//						RightMinPar->_right = RightMin->_right;
//
//					}
//					delete RightMin;
//					return true;
//
//
//				}
//			} 
//			//没有找到
//			return false;
//
//		}
//	}
//private:
//	void _printorder(Node* cur)
//	{
//		if (cur == nullptr)
//		{
//			return;
//		}
//
//		_printorder(cur->_left);
//		cout << cur->_key << " " ;
//		_printorder(cur->_right);
//	}
//	Node* _root = nullptr;
//
//};
//
//
//
//int main()
//{
//	int arr[] = { 3,54,31,2,56,634,13,1 };
//	BSTree<int> t;
//	for (auto a : arr)
//	{
//		t.insert(a);
//	}
//	t.printorder();
//	for (auto a : arr)
//	{
//		t.erase(a);
//		t.printorder();
//
//	}
//
//
//	system("pause");
//	return 0;
//}
