#include<iostream>
#include<stack>
#include<utility>
using namespace std;
int main()
{
	/*stack<string> mystack;
	mystack.emplace("hello world!");
	mystack.emplace("hello yinxinyi!");
	while (!mystack.empty())
	{
		cout << mystack.top() << endl;
		mystack.pop();
	}*/

	// 通用函数模板，接受任意类型和数量的参数
	
	stack<int> int_stack;
	int_stack.push(1);
	int_stack.push(2);
	int_stack.push(3);
	int_stack.push(4);

	while (!int_stack.empty())
	{
		cout << int_stack.top() << " ";
		int_stack.pop();
	}
	cout << endl;
	system("pause");
	return 0;
}
