#define _CRT_SECURE_NO_WARNINGS 1
#include"game.h"

void Init(G* g)
{
	g->guessnum = 5;
	g->score = 0;
	g->guan = 1;
	g->guess = 5;
	FILE* pf = fopen("point.txt", "r+");
	fscanf(pf, "%lf", &g->max);
}

void Page()
{
	G g;
	Init(&g);
	printf("\t\t\t\t########################################\n");
	printf("\t\t\t\t#############猜数字游戏>1###############\n");
	printf("\t\t\t\t#############退出游戏>0#################\n");
	//选择选项
	int num = 0;
	scanf("%d", &num);
	switch (num)
	{
	case 1:
		game(&g,1);
		break;
	case 0:
		break;
	default:
		break;
	}
}
void Imfo(G*g)
{
	system("cls");
	printf("分数:%3lf\n", g->score);
	printf("次数:%3d\n", g->guessnum);
	printf("关卡:%3d\n", g->guan);
	printf("最高分数为:%3lf\n", g->max);
}
void game(G*g,int x)
{
	//第一关生成 1~50的数据
	int num = 50;
	srand((unsigned)time(NULL));
	int answer = rand() % (num+1);

	//打印玩家信息
	Imfo(g);


	//
	printf("开猜！！！\n");
	int player = 0;
	while (player != answer&&g->guessnum!=0)
	{
		scanf("%d", &player);
		if (player < answer)
		{
			//猜的比较小的时候
			if (abs(player - answer) < 10)
			{
				g->guessnum--;
				Imfo(g);
				printf("猜小了，但是很近了！！\n");
			}
			else
			{
				g->guessnum--;
				Imfo(g);
				printf("猜小了!!!\n");
			}
		}
		else if (player > answer)
		{
			//猜的比较大的时候
			if (abs(player - answer) < 10)
			{
				g->guessnum--;
				Imfo(g);
				printf("猜大了，但是很近了！！\n");
			}
			else
			{
				g->guessnum--;
				Imfo(g);
				printf("猜大了!!!\n");
			}
		}
		else if (player == answer)//猜对了
		{

			system("cls");
			printf("恭喜你，猜对了!!!\n");
			save(g, g->score);
			Sleep(2000);
			g->guan++;
			g->score = g->score + g->guan*10*g->guessnum;

			game2(g, x);
			
		}


	}
	system("cls");
	printf("挑战失败 游戏结束!! 正确答案是>>%d\n",answer);
	//保存数据
	save(g, g->score);

/重新回到页面
	Page();
}

void game2(G* g, int x)
{
	//数据初始化
	int num = 50 + 10 * x;
	srand((unsigned)time(NULL));
	int answer = rand() % (num + 1);
	g->guess = 5;
	g->guess += g->guan / 3;
	g->guessnum = g->guess;


	//打印玩家信息
	Imfo(g);


	//
	printf("第%d关！！开猜！！！\n",x+1);
	int player = 0;
	while (player != answer && g->guessnum != 0)
	{
		scanf("%d", &player);
		if (player < answer)
		{
			//猜的比较小的时候
			if (abs(player - answer) < 10)
			{
				g->guessnum--;
				Imfo(g);
				printf("猜小了，但是很近了！！\n");
			}
			else
			{
				g->guessnum--;
				Imfo(g);
				printf("猜小了!!!\n");
			}
		}
		else if (player > answer)
		{
			//猜的比较大的时候
			if (abs(player - answer) < 10)
			{
				g->guessnum--;
				Imfo(g);
				printf("猜大了，但是很近了！！\n");
			}
			else 
			{
				g->guessnum--;
				Imfo(g);
				printf("猜大了!!!\n");
			}
		}
		else if (player == answer)
		{
			system("cls");
			printf("恭喜你，猜对了!!!\n");
			save(g, g->score);
			Sleep(2000);
			g->guan++;
			g->score = g->score+(g->guan) * 10 * g->guessnum;
			game2(g, x+1);
		}
	}
	Sleep(2000);
	exit(1);
}


void save(G* g, int x)
{
	double max = 0;
	FILE* pf = fopen("point.txt", "r+");
	fscanf(pf, "%lf", &max);

	max = max > g->score ? max : g->score;
	g->max = max;
}