#define _CRT_SECURE_NO_WARNINGS 1
#include"Heap.h"

int main()
{
	int a[] = { 40,100,50,80,70,60,20 };
	HP ph;
	HPInit(&ph);

	for (int i = 0; i < sizeof(a) / sizeof(int); i++)
	{
		HPput(&ph, a[i]);
	}
	print(&ph);
	HPDestry(&ph);

	return 0;
}
