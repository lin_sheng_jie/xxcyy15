#pragma once
#include<iostream>

namespace lsj
{
	class string
	{
	public:

		//搭建迭代器
		typedef char* iterator;
		typedef const char* const_iterator;


		//形成迭代器具
		iterator begin();
		iterator end();


		//








	private:

		//_str 即要存储的数据数组
		// _capacity 就是总内存
		//  _size 就是  数组大小  _size<=_capacity
		char* _str;
		size_t _capacity;
		size_t _size;
	};


	




}