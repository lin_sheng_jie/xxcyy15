#define _CRT_SECURE_NO_WARNINGS 1
#include"Heap.h"

void HPInit(HP* php)
{
	assert(php);
	php->a = NULL;
	php->capacity = php->size = 0;
}
void HPDestroy(HP* php)
{
	assert(php);
	free(php->a);
	php->capacity = php->size = 0;
	php ->a=NULL;

}
//向上 调整函数
void swap(HPDataType* a1, HPDataType* a2)
{
	HPDataType tmp = *a1;
	*a1 = *a2;
	*a2 = tmp;
}
void Adjustup(HP* php,int child)
{
	assert(php);

	int parent = (child - 1) / 2;

	while (child > 0)
	{
		if (php->a[child] <php->a[parent])//孩子比较小
		{
			//所以要交换
			swap(&php->a[child], &php->a[parent]);
			child = parent;
			parent = (child - 1) / 2;
		}
		else
		{
			break;
		}
	
	}

}
// 插入后保持数据是堆
void HPPush(HP* php, HPDataType x)
{
	assert(php);
	assert(php->a);
	if (php->capacity == php->size)
	{
        size_t newcapacity = php->capacity == 0 ? 4 : 2 * php->capacity;
		HPDataType* tmp = realloc(php->a, sizeof(HPDataType) * newcapacity);
		if (tmp == NULL)
		{
			perror("realloc fail!");
			return;
		}

		php->a = tmp;

		php->capacity = newcapacity;
	}
	php->a[php->size];
	php->size++;

	Adjustup(php, php->size - 1);
}
HPDataType HPTop(HP* php)
{
	return php->a[0];
}
void AdjustDown(HPDataType* a, int n, int parent)
{
	int child = parent * 2 + 1;
	while (child < n)
	{
		// 假设法，选出左右孩子中小的那个孩子
		if (child + 1 < n && a[child + 1] > a[child])
		{
			++child;
		}

		if (a[child] > a[parent])
		{
			Swap(&a[child], &a[parent]);
			parent = child;
			child = parent * 2 + 1;
		}
		else
		{
			break;
		}
	}
}

// 删除堆顶的数据
void HPPop(HP* php)
{
	assert(php);
	assert(php->a);
	swap(php->a[0], php->a[php->size - 1]);
	//交换首尾
	php->size--;
	//向下调整
	Adjustdown(php);


}

bool HPEmpty(HP* php);


