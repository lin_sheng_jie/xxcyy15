#include<iostream>
using namespace std;

class HeapOnly
{
public:

	static HeapOnly& Get()
	{

		static HeapOnly ho;
		return ho;
	}


private:
	HeapOnly(const HeapOnly&) = delete;
	HeapOnly& operator=(const HeapOnly&) = delete;
};

int main()
{

	return 0;
} 