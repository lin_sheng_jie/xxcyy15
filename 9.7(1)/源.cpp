#include <iostream>
#include<vector>
using namespace std;

int sum(int a, int b, int c)
{
    if (a == b)
        return 0;

    if ((a + 2) % c != 0)
    {
        return 1 + sum(a + 2, b, c);
    }
    else
    {
        return 1 + sum(a + 1, b, c);
    }
}
int main()
{
    int t = 0;
    cin >> t;
    vector<vector<int>> _v(t,vector<int>(3));
    int i = 0;
    while (i < t)
    {
        int a = 0;
        int b = 0;
        int c = 0;
        cin >> a >> b >> c;
        _v[i][0] = a;
        _v[i][1] = b;
        _v[i][2] = c;
        i++;
    }
    for (int j = 0; j < t; j++)
    {
        cout << sum(_v[j][0], _v[j][1], _v[j][2]);
    }
    return 0;
}