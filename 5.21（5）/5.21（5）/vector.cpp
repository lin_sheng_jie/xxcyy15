using namespace std;

#include"vector.h"

int main()
{

	my_vector<int> s1;
	s1.push_back(0);
	s1.push_back(1);
	s1.push_back(2);
	s1.push_back(3);
	
	/*for (auto i : s1)
	{
		cout << i<<" ";
	}
	cout << endl;*/
	
	my_vector<int>::iterator it = s1.begin();
	while (it!=s1.end())
	{
		cout<<*it<<endl;
		it++;
	}
	cout << endl;


	system("pause");
	return 0;
}
