#pragma once
#include<iostream>
#include<assert.h>
#include<algorithm>


template<class T>
class my_vector
{
public:
	typedef T* iterator;
	typedef const T* const_iterator;


	//析构函数
	~my_vector()
	{
		if (_start)
		{
			delete[] _start;
			_start = _finish = _end_of_storage=nullptr;
		}

	}


	//迭代器
	iterator  begin() { return _start; }
	iterator  end() { return _finish; }

	const_iterator  begin()const { return _start; }
	const_iterator  end()const { return _finish; }

	//大小 和 容量
	size_t size()
	{
		return _finish - _start;
	}
	size_t capacity()
	{
		return _end_of_storage - _start;
	}

	//尾插 尾删  插入
	void push_back(const  T&x)
	{
		/*if (_finish == _end_of_storage)
		{
			size_t _newcapacity = capacity() == 0 ? 4 : 2 * capacity();
			reserve(_newcapacity);
		}
		*_finish = x;
		++_finish;*/

		insert(end(), x);
 
	}
	void pop_back(const T& x)
	{
		assert(size() > 0);

		--_finish;
	}
	iterator insert( iterator pos, const T& x)
	{
		assert(pos >= _start && pos <=_finish);

		//如果满了
		if (_start == _finish)
		{
			//如果发生了扩容 length记录一下 _start 到pos的距离
			// 1(*)  2  3  4  5 ^ 6            5
			// 
			size_t length = pos - _start;
			size_t newcapacity = capacity() == 0 ? 4 : 2 * capacity();
			reserve(newcapacity);

			//如果发生了扩容
			//pos += length;  这样写是错的
			//因为reserve 会更新finish start end_of_sorage
			//
			pos += _start;

		}

		iterator its = _finish-1;
		while (its >= pos)
		{
			*(its + 1) = *its;
			ist--;

		}
		*pos = x;
		++_finish;



		return pos;
	}
	iterator erase(iterator pos)  
	{
		assert(pos >= _start && pos < _finish);
		iterator is = pos + 1;
		while (is< _finish)
		{
			*(is - 1) = *is;
			is++;


		}
		_finish--;


		return 
	}


	//安置空间
	void reserve(size_t n)
	{
		//空间不够
		if (n > capacity())
		{
			size_t oldsize = size();
			//开了一块空间
			T* tmp = new T[n];
			if (_start)
			{
				memcpy(tmp, _start, sizeof(T) * size());
				delete[] _start;
			}

			//然后处理一下 finish和storage
            //为什么这里finish后面跟着 size（） size（）不会改变吗
			_start = tmp;
			_finish = tmp +oldsize;
			_end_of_storage = _start + n;

		}
	}



	//下标运算符
	T& operator[](size_t n)
	{
		assert(n < size());
		return *(_start + n);
	}






private:
	iterator  _start=nullptr; 
	iterator  _finish= nullptr;
	iterator  _end_of_storage= nullptr;
};