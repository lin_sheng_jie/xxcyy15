#include"queue.h"


void QueueHead(QSL** qsl, QueueData x)
{
	QSL*newcode = (QSL*)malloc(sizeof(QSL));
	if (newcode == NULL)
	{

		perror("malloc fail!!");
		return 1;
	}
	newcode->num = x;
	newcode->next = qsl;
	qsl = newcode;


}

void QueueTail(QSL** qsl)
{
	assert(qsl);
	QSL* cur = qsl;
	QSL* pre = NULL;
	while (cur->next)
	{
		pre = cur;
		cur = cur->next;
	}
	//此时cur 到了 尾部
	pre->next = NULL;
	
	free(cur);
	cur = NULL;


}