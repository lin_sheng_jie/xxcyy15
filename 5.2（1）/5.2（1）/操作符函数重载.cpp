//#include<iostream>
//using namespace std;
//class compute
//{
//public:
//	compute(int x,int y)
//	{
//		_x = x;
//		_y = y;
//	}
//	compute(compute&d)
//	{
//		cout << "compute(compute & d)拷贝构造" << endl;
//		_x = d._x;
//		_y = d._y;
//	}
//	compute operator=(const compute&d)
//	{
//
//		cout << "compute operator=(const compute&d)操作符重载" << endl;
//		_x = d._x;
//		_y = d._y;
//		return *this;
//	}
//	~compute()
//  	{
//		cout << "~compute()构造函数析构 " << endl;
//		_x = -1;
//		_y = -1;
//	}
//private:
//	int _x;
//	int _y;
//};
//int main()
//{
//	compute d1(1, 2);
//	compute d4(3, 4);
//	compute d2(d1);
//	compute d3 = d1;
//	d1 = d4;
//	d1 = d2 = d3;
//	return 0;
//}
//
///*这个是打印结果
//compute(compute & d)拷贝构造                   compute d2(d1);
//compute(compute & d)拷贝构造                   compute d3 = d1;
//compute operator=(const compute&d)操作符重载   d1 = d4;
//compute(compute & d)拷贝构造                   d1 = d4;
//~compute()构造函数初始化                       d1 = d4;
//compute operator=(const compute&d)操作符重载   d2 = d3
//compute(compute & d)拷贝构造                   d2 = d3
//compute operator=(const compute&d)操作符重载   d1 = d2
//compute(compute & d)拷贝构造                   d1 = d2
//~compute()构造函数析构                         
//~compute()构造函数析构                         
//~compute()构造函数析构                         d3析构
//~compute()构造函数析构                         d2析构
//~compute()构造函数析构                         d4析构
//~compute()构造函数析构                         d1析构
//*/