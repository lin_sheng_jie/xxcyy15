//#pragma once
//#include<iostream>
//#include<assert.h>
//using namespace std;
//
//
//class Date
//{
//public:
//	Date(int year = 1999, int month = 1, int day);
//
//	void Print();
//
//	bool operator<(const Date& d);
//	bool operator<=(const Date& d);
//	bool operator>(const Date& d);
//	bool operator>=(const Date& d);
//	bool operator==(const Date& d);
//	bool operator!=(const Date& d);
//
//	int  GetMonthDay(int year, int month)//调用频繁 所以要内联
//	{
//		assert(month > 0 && month < 13);
//		static int MonthDayArr[13] = { -1,31,28,31,30,31,30,31,31,30,31,30,31 };
//		if (month == 2&&(year % 4 == 0 && year % 100 != 0) || (year % 400 == 0)&&month==2)
//		{
//			return 29;
//		}
//		else
//		{
//			return  MonthDayArr[month];
//		}
//	}
//
//	Date&operator+=(int day);
//	Date operator+(int day);
//
//private:
//	int _year;
//	int _month;
//	int _day;
//};