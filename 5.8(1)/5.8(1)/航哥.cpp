//#include<iostream>
//#include<string>
//#include<list>
//#include<algorithm>
//using namespace std;
//
//void test_string1()
//{
//	// 常用
//	string s1;
//	string s2("hello world");
//	string s3(s2);
//
//	// 不常用 了解
//	string s4(s2, 3, 5);
//	string s5(s2, 3);
//	string s6(s2, 3, 30);
//	string s7("hello world", 5);
//	string s8(10, 'x');
//
//	cout << s1 << endl;
//	cout << s2 << endl;
//	cout << s3 << endl;
//	cout << s4 << endl;
//	cout << s5 << endl;
//	cout << s6 << endl;
//	cout << s7 << endl;
//	cout << s8 << endl;
//
//	cin >> s1;
//	cout << s1 << endl;
//}
//
//void push_back(const string& s)
//{
//
//}
//
//void test_string2()
//{
//	// 隐式类型转换
//	string s2 = "hello world";
//	const string& s3 = "hello world";
//
//	// 构造
//	string s1("hello world");
//	push_back(s1);
//
//	push_back("hello world");
//}
//
////class string
////{
////public:
////	// 引用返回
////	// 1、减少拷贝
////	// 2、修改返回对象
////	char& operator[](size_t i)
////	{
////		assert(i < _size);
////		return _str[i];
////	}
////private:
////	char* _str;
////	size_t _size;
////	size_t _capacity;
////};
//
//void test_string3()
//{
//	string s1("hello world");
//	s1[0] = 'x';
//
//	cout << s1.size() << endl;
//	//cout << s1.length() << endl;
//
//	for (size_t i = 0; i < s1.size(); i++)
//	{
//		s1[i]++;
//	}
//	cout << endl;
//	s1[0] = 'x';
//
//	// 越界检查
//	//s1[20];
//
//	for (size_t i = 0; i < s1.size(); i++)
//	{
//		//cout << s1.operator[](i) << " ";
//		cout << s1[i] << " ";
//	}
//	cout << endl;
//
//	const string s2("hello world");
//	// 不能修改
//	//s2[0] = 'x';
//}
//
//void test_string4()
//{
//	string s1("hello world");
//
//	// 遍历方式1：下标+[]
//	for (size_t i = 0; i < s1.size(); i++)
//	{
//		cout << s1[i] << " ";
//	}
//	cout << endl;
//
//	//遍历方式2: 迭代器
//	//string::iterator it1 = s1.begin();
//	auto it1 = s1.begin();
//	while (it1 != s1.end())
//	{
//		*it1 += 3;
//
//		cout << *it1 << " ";
//		++it1;
//	}
//	cout << endl;
//
//	//遍历方式3: 范围for
//	// 底层角度，他就是迭代器
//	cout << s1 << endl;
//	for (auto& e : s1)
//	{
//		e++;
//		cout << e << " ";
//	}
//	cout << endl;
//	cout << s1 << endl;
//
//	//cout << typeid(it1).name() << endl;
//
//	list<int> lt1;
//	lt1.push_back(1);
//	lt1.push_back(2);
//	lt1.push_back(3);
//
//	list<int>::iterator it = lt1.begin();
//	while (it != lt1.end())
//	{
//		cout << *it << " ";
//		++it;
//	}
//	cout << endl;
//
//	for (auto e : lt1)
//	{
//		cout << e << " ";
//	}
//	cout << endl;
//
//	list<double> lt2;
//}
//
////template<class T>
////struct ListNode
////{
////	ListNode<T>* _next;
////	ListNode<T>* _prev;
////	T _data;
////};
////
////template<class T>
////class list
////{
////private:
////	ListNode<T>* _head;
////};
//
//void test_string5()
//{
//	const string s1("hello world");
//	//string::const_iterator it1 = s1.begin();
//	auto it1 = s1.begin();
//	while (it1 != s1.end())
//	{
//		// 不能修改
//		//*it1 += 3;
//
//		cout << *it1 << " ";
//		++it1;
//	}
//	cout << endl;
//
//	//string::const_reverse_iterator cit1 = s1.rbegin();
//	auto cit1 = s1.rbegin();
//	while (cit1 != s1.rend())
//	{
//		// 不能修改
//		//*cit1 += 3;
//
//		cout << *cit1 << " ";
//		++cit1;
//	}
//	cout << endl;
//
//	string s2("hello world");
//	string::reverse_iterator it2 = s2.rbegin();
//	//auto it2 = s2.rbegin();
//	while (it2 != s2.rend())
//	{
//		//*it2 += 3;
//
//		cout << *it2 << " ";
//		++it2;
//	}
//	cout << endl;
//}
//
//void test_string6()
//{
//	string s1("hello world");
//	cout << s1 << endl;
//
//	// s1按字典序排序
//	//sort(s1.begin(), s1.end());
//	// 第一个和最后一个参与排序
//	//sort(++s1.begin(), --s1.end());
//
//	// 前5个排序 [0, 5)
//	//sort(s1.begin(), s1.begin()+5);
//
//	cout << s1 << endl;
//}
//
//void test_string7()
//{
//	string s1("hello world");
//	cout << s1 << endl;
//
//	s1.push_back('x');
//	cout << s1 << endl;
//
//	s1.append(" yyyyyy!!");
//	cout << s1 << endl;
//
//	string s2("111111");
//
//	s1 += 'y';
//	s1 += "zzzzzzzz";
//	s1 += s2;
//	cout << s1 << endl;
//}
//
//// vector<char>
//// string
//
//int main()
//{
//	test_string7();
//
//	/*std::cout << typeid(std::string::iterator).name() << std::endl;
//	string s2("hello world");
//	cout << sizeof(s2) << endl;*/
//
//	return 0;
//}