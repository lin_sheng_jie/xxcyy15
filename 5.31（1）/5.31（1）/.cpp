#include<iostream>
#include<vector>
using namespace std;

void quick_sort(int*arr,int left,int right)
{
	////终止 迭代
	if (left >= right)
	{
		return;
	}
	////定义变量
	int begin(left);
	int end(right);
	int key(arr[left]);
	////
	
	
		while (arr[left] <= key&& left > right)
		{
			left++;
		}
		while (arr[right] >= key && left > right)
		{
			right--;
		}
	
	key = left;
	swap(arr[left], arr[right]);
	quick_sort(arr, begin, key - 1);
	quick_sort(arr, key + 1, end);
}
int main()
{
	int arr[10] = {1,4,3,5,9,7,2,5,0,8};
	quick_sort(arr, 0, 9);
	for (auto i : arr)
	{
		cout << i <<" ";
	}
	system("pause");
	return 0;
}
