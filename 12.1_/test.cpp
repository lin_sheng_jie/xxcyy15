#include"BNTree.h"


int main()
{
	vector<int> _v{ 5,1,3,7,9,-1,2,4,6,8};
	BNTree<int> tree(_v);
	BinNode<int>* x=new BinNode<int>(0);
	tree.DisplayBTree();
	cout << endl;
	cout << "这个树是空的吗 回答1是空 回答0是不空"<<endl;
	cout << tree.empty() << endl;
	cout << "展示叶子：" <<endl;
	tree.displayLeaves();
	cout << endl;
	cout << "展示高度" << endl;
	cout << tree.Height() << endl;;
	cout << "找一下4这个结点,让x等于这个的返回值，再打印一下x" << endl;
	x=tree.FindNode(4);
	cout << "x=" << x->_val << endl;
	cout << "打印一下叶子" <<endl;
	cout<<"叶子个数是："<<tree.Leaves()<<endl;
	cout << "计算一下第二层的个数" << endl;
	tree.KCount(2);
	cout << endl;
	cout << "总共有多少节点" << endl;
	cout << tree.Nodes() << endl;
	cout << "第二层的个数是？" <<endl;
	tree.levelX(2);
	tree.Print();




	return 0;
}
