#define _CRT_SECURE_NO_WARNIGNS 1
#include<stdio.h>
#include<assert.h>
void shellprintf(int* arr, int x)
{
	assert(arr);
	int i = 0;
	for (i = 0; i < x; i++)
	{

		printf("%d ", arr[i]);
	}
	printf("\n");
}
void shell(int* arr, int x)
{
	int gap = x;
	while (gap > 1)
	{
		gap /= 2;
		for (int j = 0; j < gap; j++)
		{
			for (int i = j; i < x - gap; i += gap)
			{
				int end = i;
				int tmp = arr[end + gap];
				while (end >= 0)
				{
					if (tmp < arr[end])//后面的比前面的小 所以 往前移动 所以就是 从小到大
					{
						arr[end + gap] = arr[end];
						end -= gap;
					}
					else
					{
						break;
					}
				}
				arr[end + gap] = tmp;
			}
		}
		shellprintf(arr, x);
	}
}
int main()
{
	int arr[10] = { 1,3,5,7,8,0,2,4,9,6 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	shell(arr, sz);
	return 0;
}