#pragma once
#include<iostream>

using namespace std;


namespace My_List
{
//①单个结点
	template<class T>////ListNode类模版  这个相当于就是结构体
	struct ListNode  ////当一个类的成员不想被访问限定符限制的时候，就可以用struct
	{
		ListNode* _next;
		ListNode* _prev;
		T val;

		//初始化
		ListNode(const T& date/*=T()*/) :////这里可以加一个匿名对象
			_next(nullptr),
			_prev(nullptr),
			val(date)
		{}
	};

//②迭代器 表示的意思是开头和结尾这种 因为不符合要求 所以要创建一个类  这个是个迭代器
	template<class T>
	class ListIterator
	{

		typedef ListNode<T> Node;
		typedef ListIterator<T> Self;
	public:
        ////这里默认生成的浅拷贝就够用
		ListIterator(Node* node)
			:_node(node)
		{}
		Self& operator++()////因为这个日期类++还是日期类  后缀++
		{ 
			_node = _node->_next;
			return *this;////这里*this返回的是对象本身
		}
		Self& operator--()////因为这个日期类++还是日期类后缀++
		{
			_node = _node->_prev;
			return *this;////这里*this返回的是对象本身
		}
		Self operator++(int)
		{
			Self tmp = *this;
			_node = _node->_next;
			return tmp;
		}
		Self operator--(int)
		{
			Self tmp = *this;
			_node = _node->_prev;
			return tmp;
		}
		T& operator*()
		{
			return _node->val;
		}
		bool operator!=(const Self&it)
		{
			return _node != it._node;
		}
		bool operator==(const Self& it)
		{
			return _node == it._node;
		}
		Node& operator->()
		{
			return _node->val;
		}
	private:
		Node* _node;
	};
   
//③这个是主要的链表 由①组成
	template<class T>
	class list
	{
//定义一下 这个结构体模版  和  迭代器
		typedef ListNode<T> Node;
		typedef ListIterator<T> iterator;
	
	public:
//都是各种关于链表的函数
		list()////链表的初始化
		{
			_head = new Node(T());////这里不能用0初始化 因为这个要传到上面的ListNode函数里面去 但是这个是
			                      ////T类型的所有就会很难区分到底是int还是double还是float
			                      ////这里的处理方式 就是匿名对象T()
			_head->_next = _head;
			_head->_prev = _head;////与自己相连接
		}
//迭代器
		iterator begin()
		{
			return iterator(_head->_next);////直接匿名对象就可以了
		}
		iterator end()
		{
			return iterator(_head);
		}
//尾插
		void push_back(const T& date)
		{
			Node* newnode = new Node(date);////节点创建一个新的结点
			Node* tail = _head->_prev;
			tail->_next = newnode;
			newnode->_prev = tail;
			newnode->_next = _head;
			_head->_prev = newnode;
		}
//遍历打印
		void Traversal()
		{
			Node* remhead = _head;
			iterator it = begin();
			while (it!=end())
			{
				cout << *it << " ";
				++it;
			}
			cout << endl;
		}
//计算结点个数
			void Numsize()
			{
				Node* remhead = _head->_next;////移动头结点
				int num = 1;
				while (remhead != _head)
				{
					remhead = remhead->_next;
					num++;
				}
				cout << num << endl;
			}
	private:
		Node* _head;
	};
}