#define _CRT_SECURE_NO_WARNINGS 1 
#include"queue.h"


//遵从先进先出的原则/
//队列
void QueueInit(Queue* p)
{
	assert(p);
	p->head = p->tail = NULL;
	p->size = 0;
}
void QueueDestroy(Queue* p)
{
	assert(p);
	Qnode* cur = p->head;
	while (cur)
	{
		free(cur);
		cur = cur->next;
	}
	p->head = p->tail = NULL;
	p->size = 0;
	p = NULL;
}



void QueuePut(Queue* p, QueueData x)
{
	assert(p);
	Qnode* newcode = (Qnode*)malloc(sizeof(Qnode));
	newcode->val = x;
	newcode->next = NULL;
    //若这个队列不是空的
	if (p->tail)
	{
		p->tail->next = newcode;
		p->tail = newcode;
	}
	else//如果这个队列是空的
	{
		p->tail = p->head = newcode;
		

	}
	p->size++;
}
void QueuePop(Queue* p)
{
	assert(p);
	assert(p->head != NULL);
	if (p->head->next== NULL)
	{
		free(p->head);
		p->head = p->tail = NULL;
	}
	else  //不是只有一个元素
	{
		Qnode* cue = p->head->next;
		free(p->head);
		p->head = cue;

	}

	p->size--;
}



void QueueTop(Queue* p)
{
	assert(p);
	assert(p->head!=NULL);
	return p->head->val;
}
void QueueTail(Queue* p)
{

	assert(p);
	assert(p->tail != NULL);
	return p->tail->val;
}
bool Empty(Queue* p)
{
	assert(p);
	return p->size == 0;
}
void QueueSize(Queue* p)
{
	assert(p);
	printf("\n%d", p->size);
}

void QueuePrintf(Queue* p) {
	Qnode* cur = p->head;
	while (cur)
	{
		printf("%d->", cur->val);
		cur = cur->next;
	}
}
