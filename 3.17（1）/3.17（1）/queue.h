#pragma once

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<assert.h>
#include<stdbool.h>
typedef int QueueData;
typedef struct Qnode
{
	struct Qnode* next;
	QueueData val;

}Qnode;
typedef struct queue
{
	Qnode* head;
	Qnode* tail;
	int size;
}Queue;


void QueueInit(Queue*p);
void QueueDestroy(Queue*p);
void QueuePut(Queue*p, QueueData x);
void QueuePop(Queue*p);
void QueueTop(Queue*p);
void QueueTail(Queue*p);
bool Empty(Queue* p);
void QueueSize(Queue* p);


