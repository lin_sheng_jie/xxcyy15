#include<iostream>
#include<vector>
#include<unordered_map>
using namespace std;





int main()
{
	//1.创建数据
	vector<int> _v{ 1,5,3,12,15,9 };
	int _key = 14;

	//2.创建哈希表 分别存 <数值，位置+1>
	unordered_map<int, int> _map{ 0 };

	//3.投入哈希表
	int i = 1;
	for (auto a : _v)
	{
		_map[a] = i;
		i++;
	}

	//4.查找
	for (int i = 0; i < _v.size(); i++)
	{
		if (_map[_v[i]] && _map[_key - _v[i]])
		{
			cout << "[" << i << "," << _map[_key - _v[i]] - 1 << "]" << endl;
			break;
		}
	}
	if (i == _v.size())
	{
		cout << "没有这样的一组数字" << endl;
	}

	return 0;
}