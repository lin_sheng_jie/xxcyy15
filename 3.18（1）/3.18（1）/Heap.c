#define _CRT_SECURE_NO_WARNINGS 1
#include"Heap.h"

//堆
//堆的初始化
void HPInit(HP* p)
{
	assert(p);
	p->a = NULL;
	p->capacity = p->size = 0;
}
//堆的销毁
void HPDestry(HP* p)
{
	assert(p);
	free(p->a);
	p->a = NULL;
	p->capacity = p->size = 0;
}
//堆的插入
void HPput(HP* p, HPDataType x)
{
	assert(p);
	if (p->size == p->capacity)
	{

		size_t newcapacity = p->capacity == 0 ? 4 : 2 * p->capacity;
		HPDataType* tmp = (HPDataType*)realloc(p->a, sizeof(HPDataType) * newcapacity);
		
		
		if (tmp == NULL)
	    {
			perror("realloc mail!");
			return 1;

	    }
		p->capacity = newcapacity;
		p->a = tmp;

	}
	p->a[p->size] = x;
	p->size++;
	Adjustup(p->a, p->size - 1);

}
//向上调整
void Adjustup(HPDataType* a, int child)
{ 
	int parent = (child - 1) / 2;

	while (child>0)
	{
		//孩子比家长小
		//小堆
		if (a[child] <a[parent])
		{

			swap(&a[child], &a[parent]);
			child = parent;
		}
		else
		{
			break;
		}
	}
}

void swap(HPDataType* a, HPDataType* b)
{
	HPDataType tmp = *a;
	*a = *b;
	*b = tmp;
}



void print(HP* p)
{
	assert(p);
	for (int i = 0; i < p->size; i++)
	{
		printf("%d ", p->a[i]);
	}

}

HPDataType HeatTop(HP* p)
{


}