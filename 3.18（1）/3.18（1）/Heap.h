#pragma once

#include<stdio.h>
#include<stdlib.h>
#include<assert.h>


typedef int HPDataType;

typedef struct Heap
{
	HPDataType* a;  //a就是数组
	int size;
	int capacity;
}HP;


/// <summary>
/// d堆的销毁和初始化
/// </summary>
void HPInit(HP* p);
void HPDestry(HP* p);

//堆的插入
void HPput(HP* p, HPDataType x);


void Adjustup(HP* p, HPDataType child);


//交换函数
void swap(HPDataType* a, HPDataType* b);

HPDataType HeatTop(HP* p);


void print(HP* p);