#include<iostream>
using namespace std;


class A
{
public:
	virtual void print1()
	{
		cout << "A" << endl;
	}
	virtual void print2()
	{
		cout << "B2" << endl;
	}	
	void print3()
	{
		cout << "B3" << endl;
	}

private:

};

class B:public A
{
public:
	void print1()
	{
		cout << "B1" << endl;
	}

};

void Print(A* b)
{
	b->print1();
	b->print2();
	b->print3();
}
int main()
{
	B b;
	A a;
	Print(&b);
	Print(&a);
	
	
	
	
	system("pause");
	return 0;
}
